﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ferman.Models
{
    public class Staging
    {
        public int ID { get; set; }
        [Display(Name = "Hra")]
        [Required(ErrorMessage = "Pole {0} je vyžadováno.")]
        public int PlayID { get; set; }
        [Display(Name = "Režisér")]
        [Required(ErrorMessage = "Pole {0} je vyžadováno.")]
        public int DirectorID { get; set; }
        [Display(Name = "Premiéra")]
        [DataType(DataType.Date)]
        public DateTime? Premiere { get; set; }
        [Display(Name = "Derniéra")]
        [DataType(DataType.Date)]
        public DateTime? Derniere { get; set; }
        [Display(Name = "Hra")]
        public Play Play { get; set; }
        [Display(Name = "Režisér")]
        public Person Director { get; set; }
        [Display(Name = "Role")]
        public ICollection<Role> Roles { get; set; }
        [Display(Name = "Představení")]
        public ICollection<Performance> Performances { get; set; }
    }
}
