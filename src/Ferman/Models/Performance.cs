﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Ferman.Models
{
    public class Performance
    {
        public int ID { get; set; }
        [Display(Name = "Inscenace")]
        public int StagingID { get; set; }
        [Display(Name = "Začátek")]
        [DataType(DataType.DateTime)]
        public DateTime StartsIn { get; set; }
        [NotMapped]
        public string StartsInFormat
        {
            get { return StartsIn.ToString("yyyy-MM-ddTHH:mm"); }
            set
            {
                StartsIn = Convert.ToDateTime(value);
            }
        }
        [Display(Name = "Umístění")]
        [Required(ErrorMessage = "Pole {0} je vyžadováno.")]
        [StringLength(200, ErrorMessage = "{0} může mít maximálně {1} znaků.")]
        public string Location { get; set; }
        [Display(Name = "Inscenace")]
        public Staging Staging { get; set; }
        [Display(Name = "Alternace")]
        public ICollection<Alternation> Alternations { get; set; }

        public bool CastIsSelected(int id, IQueryable<Alternation> alternations)
        {
            return alternations.Where(a => a.CastID == id).Any();
        }
    }
}
