﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ferman.Models
{
    public class Person
    {
        public int ID { get; set; }
        [Display(Name = "Jméno")]
        [StringLength(100, ErrorMessage = "{0} může mít maximálně {1} znaků.")]
        public string FirstName { get; set; }
        [Display(Name = "Příjmení")]
        [Required(ErrorMessage = "Pole {0} je vyžadováno.")]
        [StringLength(100, ErrorMessage = "{0} může mít maximálně {1} znaků.")]
        public string LastName { get; set; }
        [Display(Name = "Telefon")]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "Pole {0} neobsahuje platný email.")]
        public string Email { get; set; }
        [Display(Name = "Je členem")]
        public bool IsMember { get; set; }
        [Display(Name = "Režie")]
        public ICollection<Staging> Stagings { get; set; }
        [Display(Name = "Obsazení")]
        public ICollection<Cast> Casts { get; set; }
        [Display(Name = "Napsané hry")]
        public ICollection<Play> Written { get; set; }
        [Display(Name = "Dramatizované hry")]
        public ICollection<Play> Playwrighten { get; set; }
        public string LFName
        {
            get { return LastName + " " + FirstName; }
        }
        public string FLName
        {
            get { return FirstName + " " + LastName; }
        }
    }
}
