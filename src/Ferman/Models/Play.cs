﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ferman.Models
{
    public class Play
    {
        public int ID { get; set; }
        [Display(Name = "Spisovatel")]
        public int? WriterID { get; set; }
        [Display(Name = "Dramatik")]
        public int? PlaywrightID { get; set; }
        [Display(Name = "Název")]
        [Required(ErrorMessage = "Pole {0} je vyžadováno.")]
        [StringLength(200, ErrorMessage = "{0} může mít maximálně {1} znaků.")]
        public string Name { get; set; }
        [Display(Name = "Spisovatel")]
        [InverseProperty("Written")]
        public Person Writer { get; set; }
        [Display(Name = "Dramatik")]
        [InverseProperty("Playwrighten")]
        public Person Playwright { get; set; }
        [Display(Name = "Inscenace")]
        public ICollection<Staging> Stagings { get; set; }
    }
}
