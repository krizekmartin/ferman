﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ferman.Models
{
    public class Cast
    {
        public int ID { get; set; }
        [Display(Name = "Herec")]
        public int PersonID { get; set; }
        [Display(Name = "Role")]
        public int RoleID { get; set; }
        [Display(Name = "Herec")]
        public Person Person { get; set; }
        [Display(Name = "Role")]
        public Role Role { get; set; }
        [Display(Name = "Alternace")]
        public ICollection<Alternation> Alternations { get; set; }
    }
}
