﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Ferman.Models.AccountViewModels
{
    public class ForgotPasswordViewModel
    {
        [Required(ErrorMessage = "Pole {0} je vyžadováno.")]
        [EmailAddress(ErrorMessage = "Pole {0} neobsahuje platný email.")]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
