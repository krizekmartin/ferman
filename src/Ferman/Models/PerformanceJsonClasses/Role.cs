﻿using System.Collections.Generic;

namespace Ferman.Models.PerformanceJsonClasses
{
    public class Role
    {
        public string Name { get; set; }
        public List<Cast> Casts { get; set; }
    }
}
