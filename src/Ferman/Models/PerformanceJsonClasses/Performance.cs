﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ferman.Models.PerformanceJsonClasses
{
    public class Performance
    {
        public string Whenwhere { get; set; }
        public List<string> Alternations { get; set; }
    }
}
