﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ferman.Models.PerformanceJsonClasses
{
    public class Overview
    {
        public List<string> Roles { get; set; }
        public List<Performance> Performances { get; set; }
    }
}
