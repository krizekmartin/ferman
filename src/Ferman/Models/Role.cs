﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ferman.Models
{
    public class Role
    {
        public int ID { get; set; }
        [Display(Name = "Inscenace")]
        public int StagingID { get; set; }
        [Display(Name = "Název")]
        [Required(ErrorMessage = "Pole {0} je vyžadováno.")]
        [StringLength(200, ErrorMessage = "{0} může mít maximálně {1} znaků.")]
        public string Name { get; set; }
        [Display(Name = "Inscenace")]
        public Staging Staging { get; set; }
        [Display(Name = "Obsazení")]
        public ICollection<Cast> Casts { get; set; }
    }
}
