﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Ferman.Models.ManageViewModels
{
    public class ChangePasswordViewModel
    {
        [Required(ErrorMessage = "Pole {0} je vyžadováno.")]
        [DataType(DataType.Password)]
        [Display(Name = "Současné heslo")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "Pole {0} je vyžadováno.")]
        [StringLength(100, ErrorMessage = "{0} musí mít mezi {2} a {1} znaky.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Nové heslo")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Nové heslo znovu")]
        [Compare("NewPassword", ErrorMessage = "Hesla se neshodují.")]
        public string ConfirmPassword { get; set; }
    }
}
