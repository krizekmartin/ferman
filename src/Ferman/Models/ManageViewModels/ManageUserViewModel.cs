﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Ferman.Models.ManageViewModels
{
    public class ManageUserViewModel
    {
        [Required]
        public string ID { get; set; }
        [Required]
        public string UserName { get; set; }
        public bool IsRegistered { get; set; }
        public bool IsMember { get; set; }
        public bool IsAdministrator { get; set; }
    }
}
