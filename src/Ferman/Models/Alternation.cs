﻿namespace Ferman.Models
{
    public class Alternation
    {
        public int ID { get; set; }
        public int PerformanceID { get; set; }
        public int CastID { get; set; }
        public Performance Performance { get; set; }
        public Cast Cast { get; set; }
    }
}
