﻿using Ferman.Models;
using Microsoft.EntityFrameworkCore;

namespace Ferman.Data
{
    public class FermanContext : DbContext
    {
        public FermanContext(DbContextOptions<FermanContext> options) : base(options)
        {
        }

        public DbSet<Person> Persons { get; set; }
        public DbSet<Play> Plays { get; set; }
        public DbSet<Staging> Stagings { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Cast> Casts { get; set; }
        public DbSet<Performance> Performances { get; set; }
        public DbSet<Alternation> Alternations { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Person>().ToTable("Person");
            modelBuilder.Entity<Play>().ToTable("Play");
            modelBuilder.Entity<Staging>().ToTable("Staging");
            modelBuilder.Entity<Role>().ToTable("Role");
            modelBuilder.Entity<Cast>().ToTable("Cast");
            modelBuilder.Entity<Performance>().ToTable("Performance");
            modelBuilder.Entity<Alternation>().ToTable("Alternation");
        }
    }
}
