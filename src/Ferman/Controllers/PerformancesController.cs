using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Ferman.Data;
using Ferman.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;

namespace Ferman.Controllers
{
    [RequireHttps]
    [Authorize(Roles = "Administrator,Member")]
    public class PerformancesController : Controller
    {
        private readonly FermanContext _context;
        private readonly ILogger _logger;

        public PerformancesController(FermanContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _logger = loggerFactory.CreateLogger<PeopleController>();
        }

        // GET: Performances
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Index()
        {
            var fermanContext = _context.Performances.Include(p => p.Staging).ThenInclude(s => s.Play);
            return View(await fermanContext.AsNoTracking().ToListAsync());
        }

        // GET: Performances/Details/5
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var performance = await _context.Performances.Include(p => p.Alternations).ThenInclude(a => a.Cast).ThenInclude(c => c.Person)
                                                         .Include(p => p.Alternations).ThenInclude(a => a.Cast).ThenInclude(c => c.Role)
                                                         .Include(p => p.Staging).ThenInclude(s => s.Play)
                                                         .AsNoTracking().SingleOrDefaultAsync(m => m.ID == id);
            if (performance == null)
            {
                return NotFound();
            }

            return View(performance);
        }

        // GET: Performances/Create
        [Authorize(Roles = "Administrator")]
        public IActionResult Create()
        {
            var stagings = _context.Stagings.Include(s => s.Play).AsNoTracking();
            var staging = stagings.ToList()[15];
            ViewData["StagingID"] = new SelectList(stagings, "ID", "Play.Name", staging.ID);
            ViewData["Roles"] = _context.Roles.Include(r => r.Casts).ThenInclude(c => c.Person).Where(r => r.StagingID == staging.ID).OrderBy(r => r.ID).ToList();
            return View();
        }

        // POST: Performances/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Create([Bind("Location,StagingID,StartsIn")] Performance performance, int[] casts)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _context.Add(performance);
                    await _context.SaveChangesAsync();

                    for (var i = 0; i < casts.Length; i++)
                    {
                        _context.Add(new Alternation { PerformanceID = performance.ID, CastID = casts[i] });
                    }
                    await _context.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError(1005, "Creating performance failed", ex);
                ModelState.AddModelError(string.Empty, "P�edstaven� se nepoda�ilo vlo�it. Zkuste to znovu a pokud probl�m p�etrv�, obra�te se na administr�tora.");
            }
            ViewData["StagingID"] = new SelectList(_context.Stagings.Include(s => s.Play).AsNoTracking(), "ID", "Play.Name", performance.StagingID);
            ViewData["Roles"] = _context.Roles.Include(r => r.Casts).ThenInclude(c => c.Person).Where(r => r.StagingID == performance.StagingID).OrderBy(r => r.ID).ToList();
            return View(performance);
        }

        // GET: Performances/Edit/5
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var performance = await _context.Performances.SingleOrDefaultAsync(m => m.ID == id);
            if (performance == null)
            {
                return NotFound();
            }
            ViewData["StagingID"] = new SelectList(_context.Stagings.Include(s => s.Play).AsNoTracking(), "ID", "Play.Name", performance.StagingID);
            ViewData["Roles"] = _context.Roles.Include(r => r.Casts).ThenInclude(c => c.Person).Where(r => r.StagingID == performance.StagingID).OrderBy(r => r.ID).ToList();
            ViewData["Alts"] = _context.Alternations.Where(a => a.PerformanceID == performance.ID);
            return View(performance);
        }

        // POST: Performances/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Location,StagingID,StartsInFormat")] Performance performance, int[] casts)
        {
            if (id != performance.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var alts = _context.Alternations.Where(a => a.PerformanceID == performance.ID).ToList();

                    if (alts.Count != casts.Length)
                    {
                        throw new DbUpdateConcurrencyException("Number of casts is not same as the number of alternations in the database.", null);
                    }

                    _context.Update(performance);

                    for (var i = 0; i < casts.Length; i++)
                    {
                        if (casts[i] != alts[i].CastID)
                        {
                            alts[i].CastID = casts[i];
                            _context.Update(alts[i]);
                        }
                    }

                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PerformanceExists(performance.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["StagingID"] = new SelectList(_context.Stagings.Include(s => s.Play).AsNoTracking(), "ID", "Play.Name", performance.StagingID);
            ViewData["Roles"] = _context.Roles.Include(r => r.Casts).ThenInclude(c => c.Person).Where(r => r.StagingID == performance.StagingID).OrderBy(r => r.ID).ToList();
            ViewData["Alts"] = _context.Alternations.Where(a => a.PerformanceID == performance.ID);
            return View(performance);
        }

        // GET: Performances/Delete/5
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var performance = await _context.Performances.Include(p => p.Staging).ThenInclude(s => s.Play).SingleOrDefaultAsync(m => m.ID == id);
            if (performance == null)
            {
                return NotFound();
            }

            return View(performance);
        }

        // POST: Performances/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var performance = await _context.Performances.Include(p => p.Staging).ThenInclude(s => s.Play).SingleOrDefaultAsync(m => m.ID == id);
            _context.Performances.Remove(performance);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [HttpGet]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> CastsForStaging(int id)
        {
            var dbRoles = await _context.Roles.Include(r => r.Casts).ThenInclude(c => c.Person).Where(r => r.StagingID == id).ToListAsync();

            var roles = new List<Models.PerformanceJsonClasses.Role>();
            for (var i = 0; i < dbRoles.Count; i++)
            {
                var role = new Models.PerformanceJsonClasses.Role { Name = dbRoles[i].Name };
                var dbCasts = dbRoles[i].Casts.ToList();
                var casts = new List<Models.PerformanceJsonClasses.Cast>();
                for (var j = 0; j < dbRoles[i].Casts.Count; j++)
                {
                    casts.Add(new Models.PerformanceJsonClasses.Cast { ID = dbCasts[j].ID, Pname = dbCasts[j].Person.LFName });
                }
                role.Casts = casts;
                roles.Add(role);
            }

            return Json(roles);
        }

        [HttpGet]
        public async Task<IActionResult> Overview()
        {
            var fermanContext = _context.Stagings.Include(s => s.Play);
            return View(await fermanContext.AsNoTracking().ToListAsync());
        }

        [HttpGet]
        public async Task<IActionResult> OverviewItem(int id)
        {
            var dbRoles = await _context.Roles.Where(r => r.StagingID == id).OrderBy(r => r.ID).ToListAsync();

            var overview = new Models.PerformanceJsonClasses.Overview();
            var roles = new List<string>();
            for (var i = 0; i < dbRoles.Count; i++)
            {
                roles.Add(dbRoles[i].Name);
            }
            overview.Roles = roles;

            var dbPerfs = await _context.Performances.Include(p => p.Alternations).ThenInclude(a => a.Cast).ThenInclude(c => c.Person)
                                                     .Where(p => p.StagingID == id && p.StartsIn >= DateTime.Now).OrderBy(p => p.StartsIn).ToListAsync();

            var perfs = new List<Models.PerformanceJsonClasses.Performance>();

            for (var i = 0; i < dbPerfs.Count; i++)
            {
                var perf = new Models.PerformanceJsonClasses.Performance { Whenwhere = dbPerfs[i].StartsIn.ToString("g") + "<br />" + dbPerfs[i].Location };

                var dbAlts = dbPerfs[i].Alternations.OrderBy(a => a.Cast.RoleID).ToList();
                var alts = new List<string>();

                for (var j = 0; j < dbAlts.Count; j++)
                {
                    alts.Add(dbAlts[j].Cast.Person.FLName);
                }
                perf.Alternations = alts;
                perfs.Add(perf);
            }

            overview.Performances = perfs;

            return Json(overview);
        }

        private bool PerformanceExists(int id)
        {
            return _context.Performances.Any(e => e.ID == id);
        }
    }
}
