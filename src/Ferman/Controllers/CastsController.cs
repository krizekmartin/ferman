using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Ferman.Data;
using Ferman.Models;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;

namespace Ferman.Controllers
{
    [RequireHttps]
    [Authorize(Roles = "Administrator")]
    public class CastsController : Controller
    {
        private readonly FermanContext _context;
        private readonly ILogger _logger;

        public CastsController(FermanContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _logger = loggerFactory.CreateLogger<PeopleController>();
        }

        // GET: Casts
        public async Task<IActionResult> Index(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ViewData["RoID"] = id;
            var fermanContext = _context.Casts.Include(c => c.Person).Include(c => c.Role).Where(c => c.RoleID == id);
            return View(await fermanContext.AsNoTracking().ToListAsync());
        }

        // GET: Casts/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cast = await _context.Casts.Include(c => c.Alternations).ThenInclude(a => a.Performance).AsNoTracking().SingleOrDefaultAsync(m => m.ID == id);
            if (cast == null)
            {
                return NotFound();
            }

            return View(cast);
        }

        // GET: Casts/Create
        public IActionResult Create(int? id)
        {
            ViewData["PersonID"] = new SelectList(_context.Persons.AsNoTracking(), "ID", "LFName");

            if (id == null)
            {
                ViewData["RoleID"] = new SelectList(_context.Roles.AsNoTracking(), "ID", "Name");
            }
            else
            {
                ViewData["RoleID"] = new SelectList(_context.Roles.AsNoTracking(), "ID", "Name", id);
            }

            ViewData["RoID"] = id;            
            return View();
        }

        // POST: Casts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PersonID,RoleID")] Cast cast)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _context.Add(cast);
                    await _context.SaveChangesAsync();
                    return RedirectToAction("Index", new { id = cast.RoleID });
                }
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError(1004, "Creating cast failed", ex);
                ModelState.AddModelError(string.Empty, "Obsazen� se nepoda�ilo vlo�it. Zkuste to znovu a pokud probl�m p�etrv�, obra�te se na administr�tora.");
            }
            ViewData["PersonID"] = new SelectList(_context.Persons.AsNoTracking(), "ID", "LFName", cast.PersonID);
            ViewData["RoleID"] = new SelectList(_context.Roles.AsNoTracking(), "ID", "Name", cast.RoleID);
            ViewData["RoID"] = cast.RoleID;
            return View(cast);
        }

        // GET: Casts/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cast = await _context.Casts.SingleOrDefaultAsync(m => m.ID == id);
            if (cast == null)
            {
                return NotFound();
            }
            ViewData["PersonID"] = new SelectList(_context.Persons.AsNoTracking(), "ID", "LFName", cast.PersonID);
            ViewData["RoleID"] = new SelectList(_context.Roles.AsNoTracking(), "ID", "Name", cast.RoleID);
            ViewData["RoID"] = cast.RoleID;
            return View(cast);
        }

        // POST: Casts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,PersonID,RoleID")] Cast cast)
        {
            if (id != cast.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(cast);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CastExists(cast.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index", new { id = cast.RoleID });
            }
            ViewData["PersonID"] = new SelectList(_context.Persons, "ID", "LFName", cast.PersonID);
            ViewData["RoleID"] = new SelectList(_context.Roles, "ID", "Name", cast.RoleID);
            ViewData["RoID"] = cast.RoleID;
            return View(cast);
        }

        // GET: Casts/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cast = await _context.Casts.Include(c => c.Person).SingleOrDefaultAsync(m => m.ID == id);
            if (cast == null)
            {
                return NotFound();
            }

            return View(cast);
        }

        // POST: Casts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var cast = await _context.Casts.Include(c => c.Person).SingleOrDefaultAsync(m => m.ID == id);
            var rId = cast.RoleID;
            _context.Casts.Remove(cast);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index", new { id = rId });
        }

        private bool CastExists(int id)
        {
            return _context.Casts.Any(e => e.ID == id);
        }
    }
}
