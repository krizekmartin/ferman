﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Ferman.Controllers
{
    [RequireHttps]
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Základní informace o aplikaci.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Kontaktní informace.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
