using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Ferman.Data;
using Ferman.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;

namespace Ferman.Controllers
{
    [RequireHttps]
    [Authorize(Roles = "Administrator")]
    public class StagingsController : Controller
    {
        private readonly FermanContext _context;
        private readonly ILogger _logger;

        public StagingsController(FermanContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _logger = loggerFactory.CreateLogger<PeopleController>();
        }

        // GET: Stagings
        public async Task<IActionResult> Index()
        {
            var fermanContext = _context.Stagings.Include(s => s.Director).Include(s => s.Play);
            return View(await fermanContext.AsNoTracking().ToListAsync());
        }

        // GET: Stagings/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var staging = await _context.Stagings.Include(s => s.Director).Include(s => s.Play).Include(s => s.Roles).ThenInclude(r => r.Casts).ThenInclude(c => c.Person).AsNoTracking().SingleOrDefaultAsync(m => m.ID == id);
            if (staging == null)
            {
                return NotFound();
            }

            return View(staging);
        }

        // GET: Stagings/Create
        public IActionResult Create()
        {
            var pList = _context.Persons.OrderBy(s => s.LastName).AsNoTracking();
            ViewData["DirectorID"] = new SelectList(pList, "ID", "LFName");
            ViewData["PlayID"] = new SelectList(_context.Plays.AsNoTracking(), "ID", "Name");
            return View();
        }

        // POST: Stagings/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Derniere,DirectorID,PlayID,Premiere")] Staging staging)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _context.Add(staging);
                    await _context.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError(1002, "Creating staging failed", ex);
                ModelState.AddModelError(string.Empty, "Inscenaci se nepoda�ilo vlo�it. Zkuste to znovu a pokud probl�m p�etrv�, obra�te se na administr�tora.");
            }
            var pList = _context.Persons.OrderBy(s => s.LastName).AsNoTracking();
            ViewData["DirectorID"] = new SelectList(pList, "ID", "LFName", staging.DirectorID);
            ViewData["PlayID"] = new SelectList(_context.Plays.AsNoTracking(), "ID", "Name", staging.PlayID);
            return View(staging);
        }

        // GET: Stagings/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var staging = await _context.Stagings.SingleOrDefaultAsync(m => m.ID == id);
            if (staging == null)
            {
                return NotFound();
            }
            var pList = _context.Persons.OrderBy(s => s.LastName).AsNoTracking();
            ViewData["DirectorID"] = new SelectList(pList, "ID", "LFName", staging.DirectorID);
            ViewData["PlayID"] = new SelectList(_context.Plays.AsNoTracking(), "ID", "Name", staging.PlayID);
            return View(staging);
        }

        // POST: Stagings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Derniere,DirectorID,PlayID,Premiere")] Staging staging)
        {
            if (id != staging.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(staging);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StagingExists(staging.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            var pList = _context.Persons.OrderBy(s => s.LastName).AsNoTracking();
            ViewData["DirectorID"] = new SelectList(pList, "ID", "LFName", staging.DirectorID);
            ViewData["PlayID"] = new SelectList(_context.Plays.AsNoTracking(), "ID", "Name", staging.PlayID);
            return View(staging);
        }

        // GET: Stagings/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var staging = await _context.Stagings.Include(s => s.Director).Include(s => s.Play).SingleOrDefaultAsync(m => m.ID == id);
            if (staging == null)
            {
                return NotFound();
            }

            return View(staging);
        }

        // POST: Stagings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var staging = await _context.Stagings.SingleOrDefaultAsync(m => m.ID == id);
            _context.Stagings.Remove(staging);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool StagingExists(int id)
        {
            return _context.Stagings.Any(e => e.ID == id);
        }
    }
}
