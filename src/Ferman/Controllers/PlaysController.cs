using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Ferman.Data;
using Ferman.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;

namespace Ferman.Controllers
{
    [RequireHttps]
    [Authorize(Roles = "Administrator")]
    public class PlaysController : Controller
    {
        private readonly FermanContext _context;
        private readonly ILogger _logger;

        public PlaysController(FermanContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _logger = loggerFactory.CreateLogger<PeopleController>();
        }

        // GET: Plays
        public async Task<IActionResult> Index()
        {
            var fermanContext = _context.Plays.Include(p => p.Playwright).Include(p => p.Writer);
            return View(await fermanContext.AsNoTracking().ToListAsync());
        }

        // GET: Plays/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var play = await _context.Plays.Include(p => p.Playwright).Include(p => p.Writer).Include(p => p.Stagings).ThenInclude(p => p.Director).AsNoTracking().SingleOrDefaultAsync(m => m.ID == id);
            if (play == null)
            {
                return NotFound();
            }

            return View(play);
        }

        // GET: Plays/Create
        public IActionResult Create()
        {
            var pList = _context.Persons.OrderBy(s => s.LastName).AsNoTracking();
            ViewData["PlaywrightID"] = new SelectList(pList, "ID", "LFName");
            ViewData["WriterID"] = new SelectList(pList, "ID", "LFName");
            return View();
        }

        // POST: Plays/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,PlaywrightID,WriterID")] Play play)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _context.Add(play);
                    await _context.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError(1001, "Creating play failed", ex);
                ModelState.AddModelError(string.Empty, "Hru se nepoda�ilo vlo�it. Zkuste to znovu a pokud probl�m p�etrv�, obra�te se na administr�tora.");
            }
            var pList = _context.Persons.OrderBy(s => s.LastName).AsNoTracking();
            ViewData["PlaywrightID"] = new SelectList(pList, "ID", "LFName", play.PlaywrightID);
            ViewData["WriterID"] = new SelectList(pList, "ID", "LFName", play.WriterID);
            return View(play);
        }

        // GET: Plays/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var play = await _context.Plays.SingleOrDefaultAsync(m => m.ID == id);
            if (play == null)
            {
                return NotFound();
            }
            var pList = _context.Persons.OrderBy(s => s.LastName).AsNoTracking();
            ViewData["PlaywrightID"] = new SelectList(pList, "ID", "LFName", play.PlaywrightID);
            ViewData["WriterID"] = new SelectList(pList, "ID", "LFName", play.WriterID);
            return View(play);
        }

        // POST: Plays/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Name,PlaywrightID,WriterID")] Play play)
        {
            if (id != play.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(play);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PlayExists(play.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            var pList = _context.Persons.OrderBy(s => s.LastName).AsNoTracking();
            ViewData["PlaywrightID"] = new SelectList(pList, "ID", "LFName", play.PlaywrightID);
            ViewData["WriterID"] = new SelectList(pList, "ID", "LFName", play.WriterID);
            return View(play);
        }

        // GET: Plays/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var play = await _context.Plays.Include(p => p.Playwright).Include(p => p.Writer).SingleOrDefaultAsync(m => m.ID == id);
            if (play == null)
            {
                return NotFound();
            }

            return View(play);
        }

        // POST: Plays/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var play = await _context.Plays.SingleOrDefaultAsync(m => m.ID == id);
            _context.Plays.Remove(play);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool PlayExists(int id)
        {
            return _context.Plays.Any(e => e.ID == id);
        }
    }
}
