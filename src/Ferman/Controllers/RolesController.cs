using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Ferman.Data;
using Ferman.Models;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;

namespace Ferman.Controllers
{
    [RequireHttps]
    [Authorize(Roles = "Administrator")]
    public class RolesController : Controller
    {
        private readonly FermanContext _context;
        private readonly ILogger _logger;

        public RolesController(FermanContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _logger = loggerFactory.CreateLogger<PeopleController>();
        }

        // GET: Roles
        public async Task<IActionResult> Index(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ViewData["StaID"] = id;
            var fermanContext = _context.Roles.Where(r => r.StagingID == id);
            return View(await fermanContext.AsNoTracking().ToListAsync());
        }

        // GET: Roles/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var role = await _context.Roles.Include(r => r.Casts).ThenInclude(c => c.Person).AsNoTracking().SingleOrDefaultAsync(m => m.ID == id);
            if (role == null)
            {
                return NotFound();
            }

            return View(role);
        }

        // GET: Roles/Create
        public IActionResult Create(int? id)
        {
            if (id == null)
            {
                ViewData["StagingID"] = new SelectList(_context.Stagings.Include(s => s.Play).AsNoTracking(), "ID", "Play.Name");
            }
            else
            {
                ViewData["StagingID"] = new SelectList(_context.Stagings.Include(s => s.Play).AsNoTracking(), "ID", "Play.Name", id);
            }

            ViewData["StaID"] = id;
            return View();
        }

        // POST: Roles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,StagingID")] Role role)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _context.Add(role);
                    await _context.SaveChangesAsync();
                    return RedirectToAction("Index", new { id = role.StagingID });
                }
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError(1003, "Creating role failed", ex);
                ModelState.AddModelError(string.Empty, "Roli se nepoda�ilo vlo�it. Zkuste to znovu a pokud probl�m p�etrv�, obra�te se na administr�tora.");
            }
            ViewData["StagingID"] = new SelectList(_context.Stagings.Include(s => s.Play).AsNoTracking(), "ID", "Play.Name", role.StagingID);
            ViewData["StaID"] = role.StagingID;
            return View(role);
        }

        // GET: Roles/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var role = await _context.Roles.SingleOrDefaultAsync(m => m.ID == id);
            if (role == null)
            {
                return NotFound();
            }
            ViewData["StagingID"] = new SelectList(_context.Stagings.Include(s => s.Play).AsNoTracking(), "ID", "Play.Name", role.StagingID);
            ViewData["StaID"] = role.StagingID;
            return View(role);
        }

        // POST: Roles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Name,StagingID")] Role role)
        {
            if (id != role.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(role);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RoleExists(role.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index", new { id = role.StagingID });
            }
            ViewData["StagingID"] = new SelectList(_context.Stagings.Include(s => s.Play).AsNoTracking(), "ID", "Play.Name", role.StagingID);
            ViewData["StaID"] = role.StagingID;
            return View(role);
        }

        // GET: Roles/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var role = await _context.Roles.SingleOrDefaultAsync(m => m.ID == id);
            if (role == null)
            {
                return NotFound();
            }

            return View(role);
        }

        // POST: Roles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var role = await _context.Roles.SingleOrDefaultAsync(m => m.ID == id);
            var sId = role.StagingID;
            _context.Roles.Remove(role);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index", new { id = sId });
        }

        private bool RoleExists(int id)
        {
            return _context.Roles.Any(e => e.ID == id);
        }
    }
}
