﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ferman.Services
{
    public class AuthMessageSenderOptions
    {
        public string EmailUser { get; set; }
        public string EmailKey { get; set; }
    }
}
