﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Ferman.Migrations
{
    public partial class FermanTablesMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Person",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Email = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    IsMember = table.Column<bool>(nullable: false),
                    LastName = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Person", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Play",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(nullable: true),
                    PlaywrightID = table.Column<int>(nullable: true),
                    WriterID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Play", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Play_Person_PlaywrightID",
                        column: x => x.PlaywrightID,
                        principalTable: "Person",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Play_Person_WriterID",
                        column: x => x.WriterID,
                        principalTable: "Person",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Staging",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Derniere = table.Column<DateTime>(nullable: true),
                    DirectorID = table.Column<int>(nullable: false),
                    PlayID = table.Column<int>(nullable: false),
                    Premiere = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Staging", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Staging_Person_DirectorID",
                        column: x => x.DirectorID,
                        principalTable: "Person",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Staging_Play_PlayID",
                        column: x => x.PlayID,
                        principalTable: "Play",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Performance",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Location = table.Column<string>(nullable: true),
                    StagingID = table.Column<int>(nullable: false),
                    StartsIn = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Performance", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Performance_Staging_StagingID",
                        column: x => x.StagingID,
                        principalTable: "Staging",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Role",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(nullable: true),
                    StagingID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Role", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Role_Staging_StagingID",
                        column: x => x.StagingID,
                        principalTable: "Staging",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Cast",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    PersonID = table.Column<int>(nullable: false),
                    RoleID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cast", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Cast_Person_PersonID",
                        column: x => x.PersonID,
                        principalTable: "Person",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Cast_Role_RoleID",
                        column: x => x.RoleID,
                        principalTable: "Role",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Alternation",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    CastID = table.Column<int>(nullable: false),
                    PerformanceID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Alternation", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Alternation_Cast_CastID",
                        column: x => x.CastID,
                        principalTable: "Cast",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Alternation_Performance_PerformanceID",
                        column: x => x.PerformanceID,
                        principalTable: "Performance",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Alternation_CastID",
                table: "Alternation",
                column: "CastID");

            migrationBuilder.CreateIndex(
                name: "IX_Alternation_PerformanceID",
                table: "Alternation",
                column: "PerformanceID");

            migrationBuilder.CreateIndex(
                name: "IX_Cast_PersonID",
                table: "Cast",
                column: "PersonID");

            migrationBuilder.CreateIndex(
                name: "IX_Cast_RoleID",
                table: "Cast",
                column: "RoleID");

            migrationBuilder.CreateIndex(
                name: "IX_Performance_StagingID",
                table: "Performance",
                column: "StagingID");

            migrationBuilder.CreateIndex(
                name: "IX_Play_PlaywrightID",
                table: "Play",
                column: "PlaywrightID");

            migrationBuilder.CreateIndex(
                name: "IX_Play_WriterID",
                table: "Play",
                column: "WriterID");

            migrationBuilder.CreateIndex(
                name: "IX_Role_StagingID",
                table: "Role",
                column: "StagingID");

            migrationBuilder.CreateIndex(
                name: "IX_Staging_DirectorID",
                table: "Staging",
                column: "DirectorID");

            migrationBuilder.CreateIndex(
                name: "IX_Staging_PlayID",
                table: "Staging",
                column: "PlayID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Alternation");

            migrationBuilder.DropTable(
                name: "Cast");

            migrationBuilder.DropTable(
                name: "Performance");

            migrationBuilder.DropTable(
                name: "Role");

            migrationBuilder.DropTable(
                name: "Staging");

            migrationBuilder.DropTable(
                name: "Play");

            migrationBuilder.DropTable(
                name: "Person");
        }
    }
}
