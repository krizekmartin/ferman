﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Ferman.Data;

namespace Ferman.Migrations
{
    [DbContext(typeof(FermanContext))]
    [Migration("20170123110215_PerformancesMigration")]
    partial class PerformancesMigration
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                .HasAnnotation("ProductVersion", "1.1.0-rtm-22752");

            modelBuilder.Entity("Ferman.Models.Alternation", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CastID");

                    b.Property<int>("PerformanceID");

                    b.HasKey("ID");

                    b.HasIndex("CastID");

                    b.HasIndex("PerformanceID");

                    b.ToTable("Alternation");
                });

            modelBuilder.Entity("Ferman.Models.Cast", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("PersonID");

                    b.Property<int>("RoleID");

                    b.HasKey("ID");

                    b.HasIndex("PersonID");

                    b.HasIndex("RoleID");

                    b.ToTable("Cast");
                });

            modelBuilder.Entity("Ferman.Models.Performance", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Location")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.Property<int>("StagingID");

                    b.Property<DateTime>("StartsIn");

                    b.HasKey("ID");

                    b.HasIndex("StagingID");

                    b.ToTable("Performance");
                });

            modelBuilder.Entity("Ferman.Models.Person", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Email");

                    b.Property<string>("FirstName")
                        .HasMaxLength(100);

                    b.Property<bool>("IsMember");

                    b.Property<string>("LastName")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<string>("PhoneNumber");

                    b.HasKey("ID");

                    b.ToTable("Person");
                });

            modelBuilder.Entity("Ferman.Models.Play", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.Property<int?>("PlaywrightID");

                    b.Property<int?>("WriterID");

                    b.HasKey("ID");

                    b.HasIndex("PlaywrightID");

                    b.HasIndex("WriterID");

                    b.ToTable("Play");
                });

            modelBuilder.Entity("Ferman.Models.Role", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.Property<int>("StagingID");

                    b.HasKey("ID");

                    b.HasIndex("StagingID");

                    b.ToTable("Role");
                });

            modelBuilder.Entity("Ferman.Models.Staging", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime?>("Derniere");

                    b.Property<int>("DirectorID");

                    b.Property<int>("PlayID");

                    b.Property<DateTime?>("Premiere");

                    b.HasKey("ID");

                    b.HasIndex("DirectorID");

                    b.HasIndex("PlayID");

                    b.ToTable("Staging");
                });

            modelBuilder.Entity("Ferman.Models.Alternation", b =>
                {
                    b.HasOne("Ferman.Models.Cast", "Cast")
                        .WithMany("Alternations")
                        .HasForeignKey("CastID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Ferman.Models.Performance", "Performance")
                        .WithMany("Alternations")
                        .HasForeignKey("PerformanceID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Ferman.Models.Cast", b =>
                {
                    b.HasOne("Ferman.Models.Person", "Person")
                        .WithMany("Casts")
                        .HasForeignKey("PersonID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Ferman.Models.Role", "Role")
                        .WithMany("Casts")
                        .HasForeignKey("RoleID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Ferman.Models.Performance", b =>
                {
                    b.HasOne("Ferman.Models.Staging", "Staging")
                        .WithMany("Performances")
                        .HasForeignKey("StagingID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Ferman.Models.Play", b =>
                {
                    b.HasOne("Ferman.Models.Person", "Playwright")
                        .WithMany("Playwrighten")
                        .HasForeignKey("PlaywrightID");

                    b.HasOne("Ferman.Models.Person", "Writer")
                        .WithMany("Written")
                        .HasForeignKey("WriterID");
                });

            modelBuilder.Entity("Ferman.Models.Role", b =>
                {
                    b.HasOne("Ferman.Models.Staging", "Staging")
                        .WithMany("Roles")
                        .HasForeignKey("StagingID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Ferman.Models.Staging", b =>
                {
                    b.HasOne("Ferman.Models.Person", "Director")
                        .WithMany("Stagings")
                        .HasForeignKey("DirectorID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Ferman.Models.Play", "Play")
                        .WithMany("Stagings")
                        .HasForeignKey("PlayID")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
